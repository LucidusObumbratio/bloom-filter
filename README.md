The implementation of Bloom Filter.
____
You can change values:

* n - numbers in set
* range - range for random values
* factor - how many bits will be used for every number in set
* k - number of hash functions