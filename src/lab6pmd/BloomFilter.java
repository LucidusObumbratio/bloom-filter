/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab6pmd;

import java.util.HashSet;
import java.util.Random;

/**
 *
 * @author Maciek
 */
public class BloomFilter {

    int size = 0;
    int k = 0;
    int primal = 0;
    boolean tab[];
    long a[];
    long b[];
	
	public BloomFilter(int size, int k, int range) {

		this.size = size;
		this.k = k;
		
                this.tab = new boolean[size];
                
                for(int i=0; i<this.tab.length; i++)
                {
                    this.tab[i] = false;
                }
                
                // Get primal
                this.primal = range+1;
                
                while(true)
                {
                    boolean p = true;
                    for(int i = 2; i <= Math.sqrt(range); i++)
                    {
                        if (this.primal % i == 0)
                        {
                            p = false;
                            break;
                        }
                            
                    }
                    if (p)
                        break;
                    
                    this.primal++;
                }
                
                //System.out.println(this.primal);
                
                this.a = new long[this.k];
                this.b = new long[this.k];
                
                Random r = new Random(0xdeadbeef);
                for(int i=0; i < this.k; i++)
                {
                    a[i] = r.nextInt(primal-1) + 1;
                    b[i] = r.nextInt(primal);
                }
	}
	
	public void add(int key) {
		for(int i =0 ; i <this.k; i++)
                {
                    long hv = 0;
                    hv = (long) this.a[i] * key + this.b[i];
                    
                    hv %= this.primal;
                    hv %= this.size;
                    
                    this.tab[(int)hv] = true;
                }
	}
	
	public Boolean contains(int key) {
		
		for(int i =0; i <this.k; i++)
                {
                    long hv = 0;
                    hv = (long) this.a[i] * key + this.b[i];
                    hv %= this.primal;
                    hv %= this.size;
                    
                    if(!this.tab[(int)hv])
                        return false;
                }
		return true;
	}
	
	public static void main(String[] args) {
		
		int n = 10000; 
		int range = 100000000;
		double factor = 20;
		int size = (int) Math.round(factor * n);

		int k = 7;
		
		Random random = new Random(0xdeadbeef);
		
		BloomFilter bf = new BloomFilter(size, k, range);
		
		HashSet<Integer> set = new HashSet<Integer>(n);
		
		while(set.size() < n) {
			set.add(random.nextInt(range));
		}
		
		for(int item : set) {
			bf.add(item);
		}
		
		int TP = 0, FP = 0, TN = 0, FN = 0;
		
		for(int i = 0; i < range; i++) {
			int key = i; //random.nextInt(range);
			Boolean containsBF = bf.contains(key);
			Boolean containsHS = set.contains(key);
			
			//System.out.println(key + " " + containsBF + " " + containsHS);
			
			if(containsBF && containsHS) {
				TP++;
			} else if(!containsBF && !containsHS) {
				TN++;
			} else if(!containsBF && containsHS) {
				FN++;
			}  else if(containsBF && !containsHS) {
				FP++;
			}   
		}
		
		System.out.println("TP = " + String.format("%6d", TP) + "\tTPR = " + String.format("%1.4f", (double) TP/ (double) n));
		System.out.println("TN = " + String.format("%6d", TN) + "\tTNR = " + String.format("%1.4f", (double) TN/ (double) (range-n)));
		System.out.println("FN = " + String.format("%6d", FN) + "\tFNR = " + String.format("%1.4f", (double) FN/ (double) (n)));
		System.out.println("FP = " + String.format("%6d", FP) + "\tFPR = " + String.format("%1.4f", (double) FP/ (double) (range-n)));
		
	}
    
}
